const getBunny = ()  => {
    let url="https://api.bunnies.io/v2/loop/random/?media=gif,png";

    fetch(url).then(function (response) {
	// The API call was successful!
	if (response.ok) {
		return response.json();
	}
	// There was an error
	return Promise.reject(response);

    }).then(function (data) {
        // This is the JSON from our response
        let img = document.createElement('img');
        img.src = data.media.gif;
        img.className = "bunny-picture"
        let content = document.querySelector(".content");
        content.append(img);

    }).catch(function (err) {
        // There was an error
        console.warn('Something went wrong.', err);
    });  
}

getBunny();